#O2-Opt-In
A image heavy microsite encouraging customers opting into a O2 rewards.

http://o2resign.s3-website-eu-west-1.amazonaws.com/

Stack
-------
- Client: 
    * Backbone: http://backbonejs.org/
    * Marionette: https://github.com/marionettejs/backbone.marionette
    * jQuery: http://jquery.com/
    * Require: http://requirejs.org/
    * Handlebars: 
        - http://handlebarsjs.com/
        - https://github.com/SlexAxton/require-handlebars-plugin
    * SASS-Bootstrap:
        - http://twitter.github.io/bootstrap
        - https://github.com/thomas-mcdonald/bootstrap-sass
    * Bourbon: http://bourbon.io/
- Tooling: 
    * Yeoman: http://yeoman.io/
    * Bower: http://bower.io/
    * Grunt: http://gruntjs.com/
- Testing:
    * phantomJS http://phantomjs.org/
    * Mocha http://visionmedia.github.io/mocha/
    * Chai http://chaijs.com/
    * Sinon http://sinonjs.org/


## ANIMATION LEGEND ##

    * frames: total number of images
    * entrySpeed: speed that the animation will run at first time round.
    * shy: images are loaded on request (click). Left this as variable content as we may want to tweak if load speed is an issue
    * repeatDelay: if the animation is to loop we may want to wait before we go again
    * stopFrame: non looping animations need to know where to stop (or it will go back round to O)
    * speed: obvious, but animations will always loop if this property is present
    * dwellTime: how long we sit on a frame before we auto scroll