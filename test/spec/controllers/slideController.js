(function() {
    'use strict';

    var root = this;

    root.define([
            'controllers/slideController'
        ],
        function(Slidecontroller) {

            describe('Slidecontroller Controller', function() {

                it('should be an instance of Slidecontroller Controller', function() {
                    var slideController = new Slidecontroller();
                    expect(slideController).to.be.an.instanceof(Slidecontroller);
                });

            });

        });

}).call(this);