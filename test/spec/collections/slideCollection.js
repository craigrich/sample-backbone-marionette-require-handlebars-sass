(function() {
    'use strict';

    var root = this;

    root.define([
            'collections/slideCollection'
        ],
        function(Slidecollection) {

            describe('Slidecollection Collection', function() {

                it('should be an instance of Slidecollection Collection', function() {
                    var slideCollection = new Slidecollection();
                    expect(slideCollection).to.be.an.instanceof(Slidecollection);
                });


            });

        });

}).call(this);