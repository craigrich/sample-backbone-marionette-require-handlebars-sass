define(function() {
	'use strict';

	/* return an array of specs to be run */
	return {
		specs: ['spec/collections/slideCollection.js',
		'spec/controllers/slideController.js',
		'spec/exampleTest.js',
		'spec/models/slideModel.js',
		'spec/views/composite/slideCompositeView.js',
		'spec/views/item/footerView.js',
		'spec/views/item/headerView.js',
		'spec/views/item/slideView.js'
		]
	};
});
