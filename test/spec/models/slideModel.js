(function() {
    'use strict';

    var root = this;

    root.define([
            'models/slideModel'
        ],
        function(Slidemodel) {

            describe('Slidemodel Model', function() {

                it('should be an instance of Slidemodel Model', function() {
                    var slideModel = new Slidemodel();
                    expect(slideModel).to.be.an.instanceof(Slidemodel);
                });


            });

        });

}).call(this);