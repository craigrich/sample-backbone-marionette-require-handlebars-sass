(function() {
    'use strict';

    var root = this;

    root.define([
            'views/composite/slideCompositeView'
        ],
        function(Slidecompositeview) {

            describe('Slidecompositeview Compositeview', function() {

                it('should be an instance of Slidecompositeview Compositeview', function() {
                    var slideCompositeView = new Slidecompositeview();
                    expect(slideCompositeView).to.be.an.instanceof(Slidecompositeview);
                });

            });

        });

}).call(this);