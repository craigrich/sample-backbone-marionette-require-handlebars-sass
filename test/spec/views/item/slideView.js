(function() {
    'use strict';

    var root = this;

    root.define([
            'views/item/slideView'
        ],
        function(Slideview) {

            describe('Slideview Itemview', function() {

                it('should be an instance of Slideview Itemview', function() {
                    var slideView = new Slideview();
                    expect(slideView).to.be.an.instanceof(Slideview);
                });

            });

        });

}).call(this);