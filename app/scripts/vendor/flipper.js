(function($, window, document, undefined) {

    'use strict';

    // Create the defaults once
    var pluginName = 'flipper',
        defaults = {
            speed: 3333,
            faces: 6,
            endNum: '561267' // arbitrary
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {

        init: function() {
            var self = this;
            self.$el = $(this.element);
            self.renderFace();

            $.each(new Array(self.settings.faces), function() {
                self.$el.append(self.renderFace());
            });

            var digits = self.settings.endNum.split('');

            setTimeout(function() {
                $('.flip').each(function(index) {
                    $(this).children('li:nth-child('+ digits[index] +')').addClass('active');
                })
            }, 100)

        },

        setCounter: function(count) {
            var target = count.split('');

            var timer = setInterval(function() {
                tick();
            }, 100);


            function tick() {
                $('.flip').each(function(index) {

                    var $faceNumber = $(this).children('li.active');

                    if ($faceNumber.is(':last-child')) {
                        $(this).children().removeClass('before');
                        $faceNumber.addClass('before').removeClass('active');
                        $faceNumber = $(this).children().eq(0);
                        $faceNumber.addClass('active');
                    } else {
                        $(this).children().removeClass('before');
                        $faceNumber.addClass('before')
                            .removeClass('active')
                            .next('li')
                            .addClass('active');   
                    }
                });
            }

            setTimeout(function() {
                clearInterval(timer); 
                $('.flip').each(function(index) {
                    var idx = Math.abs(target[index])+1;
                    $(this).children('li').removeClass('active');
                    $(this).children('li:nth-child('+ idx +')').addClass('active').removeClass('before');
                })
            }, 1000)
        },
       
        renderFace: function() {
            var $face = $('<ul class="flip"/>');
            $.each(new Array(10), function(index) {
                $face.append('<li><a href="#"><div class="up"><div class="shadow"></div><div class="inn">' + index + '</div></div><div class="down"><div class="shadow"></div> <div class="inn">' + index + '</div></div></a></li>');
            });
            return $face;
        },

    });


    $.fn[pluginName] = function(options) {
        return new Plugin(this, options);
    };

})(jQuery, window, document);