define([
        'jquery',
        'backbone',
        'views/item/headerView',
        'views/item/footerView',
        'controllers/slideController'
    ],

    function($, Backbone, HeaderView, FooterView, SlideController) {

        'use strict';

        var App = new Backbone.Marionette.Application();

        var headerModel = new Backbone.Model({
            'msg': '',
            'count': ''
        });

        App.addRegions({
            headerRegion: '#header-region',
            mainRegion: '#content-region',
            footerRegion: '#footer-region'
        });

        App.addInitializer(function() {
            var isSmallDevice = (Modernizr.mq('only screen and (max-device-width : 320px) and (orientation:portrait)') || Modernizr.mq('only screen and (max-device-width : 480px) and (orientation:landscape)')) && !Modernizr.mq('only screen and (device-aspect-ratio: 40/71)');
            if (isSmallDevice) {
                $('body').addClass('smallDevice');
            }
            App.headerRegion.show(new HeaderView({
                model: headerModel
            }));
            App.footerRegion.show(new FooterView());
            new SlideController().showMainRegion();
        });

        return App;
    });



//For reference
//Communicator.mediator.trigger('APP:START');