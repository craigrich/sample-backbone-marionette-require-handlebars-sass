var utils;

(function () {
	'use strict';

	utils = {

		track: function(data) {
			window.ga('send', 'event', data.type, data.ev, data.val);
		}

	};

})();