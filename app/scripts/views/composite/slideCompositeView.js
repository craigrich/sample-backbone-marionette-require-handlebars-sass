define([
        'underscore',
        'swiper',
        'backbone',
        'views/item/slideView',
        'hbs!tmpl/composite/slideCompositeView_tmpl',
        'communicator',
        'utils'
    ],
    function(_, Swiper, Backbone, SlideView, SlidecompositeviewTmpl, Communicator) {
        'use strict';

        /* Return a CompositeView class definition */
        return Backbone.Marionette.CompositeView.extend({

            className: 'swiper-container',

            template: SlidecompositeviewTmpl,

            /* where are we appending the items views */
            itemViewContainer: '.swiper-wrapper',

            //An individual slide
            itemView: SlideView,

            //The Main Swiper
            swiper: '',

            timers: [],

            initialize: function() {
                _.bindAll(this, 'renderSwiper', 'animateSlide');
                this.bindToMediator();
            },

            onRender: function() {
                var self = this;
                setTimeout(function() {
                    self.renderSwiper();
                }, 50);
            },

            bindToMediator: function() {
                var self = this;
                Communicator.mediator.on('FORM:SUCCESS', function() {
                    var activeSlideView = self.children.findByIndex(self.swiper.activeIndex);
                    activeSlideView.showThanks();
                    self.swiper.destroy();

                    var trackingData = {
                        type: 'form',
                        ev: 'form-post',
                        val: 'success'
                    };
                    utils.track(trackingData);
                });

                Communicator.mediator.on('CONTENT:ADVANCE', function() {
                    self.clearTimers();
                    self.swiper.swipeNext();
                });

                Communicator.mediator.on('CONTENT:ADDTIMER', function(timer) {
                    self.timers.push(timer);
                });

            },

            //Sets up a swiper instance and bind callbacks
            renderSwiper: function() {
				var self = this;
                this.swiper = this.$el.swiper({
                    mode: 'horizontal',
                    initialSlide: '0',
                    speed: 500,
                    //onSwiperCreated: this.animateSlide,
					onSwiperCreated: function(swiper) {
						$(".o2__content").addClass("show");
						self.animateSlide(swiper);
					},
                    onSlideChangeStart: this.animateSlide
                });
            },

            animateSlide: function(swiper) {
                this.clearTimers();
                var totalSlides = swiper.slides.length - 1,
                    currentSlideView = swiper.activeIndex,
                    activeSlideView = this.children.findByIndex(currentSlideView);

                activeSlideView.animate(totalSlides, currentSlideView);

                var trackingData = {
                    type: 'nav',
                    ev: 'show-slide',
                    val: 'slide: ' + (currentSlideView + 1)
                };
                utils.track(trackingData);
            },

            clearTimers: function() {
                if (!this.timers.length) {
                    return;
                }
                for (var i = 0; i < this.timers.length; i++) {
                    clearTimeout(this.timers[i]);
                }
                this.timers = [];
            },

        });

    });