define([
        'backbone',
        'hbs!tmpl/item/footerView_tmpl',
        'communicator',
        'parse'
    ],
    function(Backbone, FooterviewTmpl, Communicator, Parse) {
        'use strict';

        return Backbone.Marionette.ItemView.extend({

            template: FooterviewTmpl,
            className: 'footer-wrapper hide',

            events: {
                'click button': 'postData',
                'click .css-label': 'toggleCheckbox'
            },

            initialize: function() {
                Parse.initialize('OlyHaRBOowGOEMQ8f0QKyyPFh44hfIISQnJN1imE', 'y2MWDx8W6ChBsxynXdJPDVjNA7R7YQFQzLOaVHP7');
                this.getNumber();
				var self = this;
				Communicator.mediator.on('FOOTER:MONTH', function(data) {
					if(data) {
						self.$el.find('.month').text(data).parent().show();
					} else {
						self.$el.find('.month').parent().hide();
					}
                });
				Communicator.mediator.on('FOOTER:HIDE', function(data) {
					self.$el.addClass('hide');
                });
				Communicator.mediator.on('FOOTER:SHOW', function(data) {
					self.$el.removeClass('hide');
                });
            },

            getNumber: function() {
                return this.phoneNumber = window.location.search.substr(1).split('=')[1] * 1;
            },

            toggleCheckbox: function() {
                console.log('checking ze box');
            },

            postData: function(ev) {
                ev.preventDefault();
                var self = this;

                //Add error message not checked
                if (!this.$('form #checkboxG4').is(':checked') || isNaN(this.phoneNumber)) {
                    this.$el.find('.error').addClass('show');
                    setTimeout(function() {
                        self.$el.find('.error').removeClass('show');
                    }, 3000);
                } else {

                    var OptIn = Parse.Object.extend('opt_ins');
                    var _optIn = new OptIn();

                    _optIn.save({
                        number: this.phoneNumber
                    }, {
                        success: function() {
                            Communicator.mediator.trigger('FORM:SUCCESS');
                            Communicator.mediator.trigger('HEADER:HIDE');
                            self.$el.addClass('hide');
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                }
            },

        });
    });