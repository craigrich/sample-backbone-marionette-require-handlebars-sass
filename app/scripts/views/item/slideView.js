define([
        'backbone',
        'communicator',
        'hbs!tmpl/item/slideView_tmpl',
        'hbs!tmpl/item/startView_tmpl'
    ],
    function(Backbone, Communicator, SlideviewTmpl, StartView) {

        'use strict';

        /* Return a ItemView class definition */
        return Backbone.Marionette.ItemView.extend({

            className: 'swiper-slide ',

            //Comment this out if you want no animations on iphone 4
            // isIphone4: window.screen.height === (960 / 2),
            // template: window.screen.height === (960 / 2) ? Iphone4View : SlideviewTmpl,

			
            template: SlideviewTmpl,

            initialize: function() {
			
				if(this.model.get("StartPage")) {
					this.template = StartView;
				}
				
                require(['jquery.reel']);
            },

            //Called from the compositeView Swiper
            animate: function(total, id) {
                var self = this,
                    stopFrame = this.model.get('stopFrame'),
                    replay = this.model.get('replay'),
                    count = this.model.get('count'),
                    message = this.model.get('message');
				
				if( this.model.get("StartPage") ) {
					Communicator.mediator.trigger('FOOTER:HIDE');
					console.log("before");
					self.frameTimer(self.model.get('dwellTime') * 1000);					
				} else {
					Communicator.mediator.trigger('FOOTER:SHOW');				
				}				
				
                if (total === id || this.model.get("StartPage")) {
                    Communicator.mediator.trigger('HEADER:HIDE');
                } else {
                    Communicator.mediator.trigger('HEADER:ANIMATE', count, message);
                    this.$('.reel').bind('loaded', function() {
                        self.frameTimer(self.model.get('dwellTime') * 1000);
                    });
                }
				Communicator.mediator.trigger('FOOTER:MONTH', this.model.get("month"));
                //Comment this out if you want no animations on iphone 4
                // if (this.isIphone4) {
                //     return;
                // }
                // 

                self.$('.reel').trigger('click');
                if (stopFrame) {
                    this.$('.reel').trigger('reach', stopFrame);
                }
            },

            showThanks: function() {
                this.$('.reel').addClass('hide');
                this.$('.thanks').addClass('show');
            },

            frameTimer: function(delay) {
                var timer = setTimeout(function() {
                    Communicator.mediator.trigger('CONTENT:ADVANCE');
					console.log("here");
                }, delay);
                Communicator.mediator.trigger('CONTENT:ADDTIMER', timer);
            },
        });

    });