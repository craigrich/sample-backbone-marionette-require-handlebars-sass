define([
        'backbone',
        'communicator',
        'flipper',
        'hbs!tmpl/item/headerView_tmpl'
    ],
    function(Backbone, Communicator, Flipper, HeaderviewTmpl) {
        //'use strict';

        /* Return a ItemView class definition */
        return Backbone.Marionette.ItemView.extend({

            template: HeaderviewTmpl,

            ui: {},

            events: {},

            modelEvents: {
                'change': 'setContent'
            },

            className: 'o2__header hide',

            /* on render callback */
            onRender: function() {
                this.bindMediatorActions();
                this.$flipper = this.$('#js-flipper').flipper();
            },

            bindMediatorActions: function() {
                var self = this;
                Communicator.mediator.on('HEADER:ANIMATE', function(count, msg) {
                    self.model.set({
                        'msg': msg,
                        'count': count
                    });
                    self.$el.removeClass('hide');
                });

                Communicator.mediator.on('HEADER:HIDE', function() {
                    self.$el.addClass('hide');
                });
				
				Communicator.mediator.on('HEADER:SHOW', function() {
                    self.$el.removeClass('hide');
                });
            },

            setContent: function() {
                this.$('.o2__header__content p').html(this.model.get('msg'));
                this.$flipper.setCounter(this.model.get('count'));
            },


        });

    });