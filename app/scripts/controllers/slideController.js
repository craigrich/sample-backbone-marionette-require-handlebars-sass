define([
        'underscore',
        'backbone',
        'collections/slideCollection',
        'views/composite/slideCompositeView'
    ],
    function(_, Backbone, SlideCollection, SlideCompositeView) {

        'use strict';
        return Backbone.Marionette.Controller.extend({

            slideCollection: '',

            //Fetch all json before rendering a collection of slides
            initialize: function() {
                _.bindAll(this, 'renderSlides');
            },

            showMainRegion: function() {
                this.slideCollection = new SlideCollection();
                this.slideCollection.fetch().done(this.renderSlides);
            },

            //Render the Composite view
            renderSlides: function() {
                var App = require('application');
                App.mainRegion.show(new SlideCompositeView({
                    collection: this.slideCollection
                }));
            }

        });

    });