define([
        'backbone',
        'models/slideModel'
    ],
    function(Backbone, Slidemodel) {
        'use strict';

        return Backbone.Collection.extend({

            url: 'data.json',
            model: Slidemodel

        });
    });