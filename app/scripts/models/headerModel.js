define([
        'backbone'
    ],
    function(Backbone) {
        'use strict';

        return Backbone.Model.extend({
            defaults: {
                'count': 100000,
                'msg': 'x'
            }
        });
    });