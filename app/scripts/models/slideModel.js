define([
        'backbone'
    ],
    function(Backbone) {
        'use strict';

        /* Return a model class definition */
        return Backbone.Model.extend({
            initialize: function() {

            },

            defaults: {
                'count': 1000,
                'message': 'This is a default message',
                'imagePath': 'images/someDirectory',
            },

        });
    });